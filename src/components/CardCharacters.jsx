import React from 'react'
import { Link } from 'react-router-dom';
import {CardColumns, Card, Button} from 'react-bootstrap';
import DetailOverView from '../pages/DetailOverview';
import peoplePic from './Assets/people.jpeg';

function CardCharacters({item}) {
    console.log(item)

    return (
        <div>
                <div>                
                    <Card className="home-col movie-card" sm={1} md={2} lg={4} id={item.id}>
                      <Card.Img variant="top" src={item.profile_path !== null ? `https://team-c.gabatch11.my.id${item.image}` : peoplePic}/>
                      <Card.Body className='text-center'>
                        <Card.Title><strong>{item.name}</strong></Card.Title>
                        <Card.Text>As a voice over</Card.Text>
                      </Card.Body>
                    </Card>
                </div>

        </div>
    )
}

export default CardCharacters
