import {useState, useEffect, useReducer, useContext} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getCharactersMovie } from '../store/actions/movieData';
import { getDataMovie } from '../store/actions/movieData';
import {CardColumns, Card, Button} from 'react-bootstrap';

import CardCharacters from './CardCharacters';

const MainDetailCharacters = () => {

    const dispatch = useDispatch();

    const id = window.location.pathname.slice(-24);

    const movieData = useSelector((state) => state.movieData.movies);
    const charData = useSelector((state) => state.charsMovieData.charsMovies);
    // console.log('charac', charData);
  
    useEffect(() => {
    dispatch(getDataMovie());
    dispatch(getCharactersMovie(id));
    }, [])


    return (
    <div className="container characters">
        <div className="category">
            <div className="category-list d-flex flex-wrap">
            <a href={`/DetailOverview/${id}`}>Overview</a>
            <a className="active" href={`/DetailCharacters/${id}`}>Characters</a>
            <a href={`/DetailReview/${id}`}>Review</a>
            </div>
        </div>
        <div className="container">
        <div className="container">
        <div className="movie-list-pic">

          <CardColumns>
          {
           charData.map((item, i) => {
              return(
             
              <CardCharacters
              key={i}
              item={item}
              />

              );       
            })
          }

          </CardColumns>
        </div>

      </div>
        </div>
    </div>
    )
}

export default MainDetailCharacters;
