import { getAllByAltText } from '@testing-library/dom';
import React from 'react';
import {useState, useEffect, useReducer, useContext} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getDataMovie, getDetailMovie } from '../store/actions/movieData';
import Moment from 'moment';
import NumberFormat from 'react-number-format';

const OverviewMovie = () => {

const id = window.location.pathname.slice(-24);

const dispatch = useDispatch();
const detailMovieData = useSelector((state) => state.detailMovieData.detailMovies);
console.log('cek detailMovieData', detailMovieData);

  useEffect(() => {
    dispatch(getDetailMovie(id));
}, [])


    return(
    <div className="container overview">
        <div className="category">
            <div className="category-list d-flex flex-wrap">
            <a className="active" href={`/DetailOverview/${id}`}>Overview</a>
            <a href={`/DetailCharacters/${id}`}>Characters</a>
            <a href={`/DetailReview/${id}`}>Review</a>
            </div>
        </div>
        <div className="container">
        <div>
            <h1>{detailMovieData.title}</h1>
            <p>{detailMovieData.synopsis}</p>
        </div>
        <div className="movieinfo">
            <h1>Movie Info</h1>
            <ul className="movieinfo-list">
                <li>Release year : {
                //Moment(detailMovieData.releaseYear).format('MM-DD-YYYY')
                detailMovieData.releaseYear
               }</li>
                <li>Author : </li>
                <li>Studios : {detailMovieData.studios}</li>
                <li>Featured song : {detailMovieData.tagline}</li>
                <li>Budget : {
                <NumberFormat value={detailMovieData.budget} displayType={'text'} thousandSeparator={true} prefix={'$'} />
                }</li>
            </ul>
        </div>
        </div>
    </div>  
    );
}

export default OverviewMovie;
