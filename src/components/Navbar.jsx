import React, {useState} from "react"
import {Navbar, Image } from "react-bootstrap"
import Modal from "./ModalPopOut"
import SearchMovie from "./Search"
import img1 from "./Assets/brandLogo.png"
import {Dropdown} from "react-bootstrap"
import "../pages/Home.css"
import { useHistory } from "react-router"

const Navigation = () => {
  //untuk modal
  const [show, setShow] =useState(false)
  const [showRegister, setShowRegister] = useState(false)
  // const [logIn, setLogIn] = useState(true)
  const token = localStorage.getItem("token")
  // const profileIcon = localStorage
  const history = useHistory()

  
  const showModalSignIn =() => {
    setShow(!show);
  }
  const showModalRegister = () => {
    setShowRegister(!showRegister);
  }

  const triggerSignIn = ()  => {
    setShowRegister(false);
    return setShow(true);
  }

  const triggerRegister = () => {
    setShow(false);
    return setShowRegister(true);
  }

  const signOut = () => {
    localStorage.clear();
    history.push("/")
  }
  
  return(
    <>
    <Navbar className="navbar" bg="light" expand="lg">
      <div className="rectangle">
        <a href="/">
        <img src={img1} alt="logo" className="main-logo" />
        </a>
      </div>
      <Navbar.Brand className="title" href="/">Meowme</Navbar.Brand>
      <SearchMovie />
      {
      token? 
      <div className="profile-menu-icon">
        <Dropdown>
          <Dropdown.Toggle  className="dropdown-basic">           
            <Image src= {token} roundedCircle alt="profile"/>
          </Dropdown.Toggle>
          <Dropdown.Menu >
            <Dropdown.Item href="/DetailCharacters">Characters</Dropdown.Item>
            <Dropdown.Item href="/DetailOverview">Overview</Dropdown.Item>
            <Dropdown.Item href="/DetailReview">Review</Dropdown.Item>
            <Dropdown.Item href="/Profile">Profile</Dropdown.Item>
            <Dropdown.Item href="/" onClick={signOut}>Sign out</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </div> : 
      <button className= "sign-in" onClick={showModalSignIn}>Sign In</button>} 
    </Navbar>
    <Modal show ={show} triggerSignIn={triggerSignIn} showRegister={showRegister} showModalRegister={showModalRegister} triggerRegister={triggerRegister} setShow= {setShow} setShowRegister={setShowRegister}/>
    </>
  ) 
}

export default Navigation
    