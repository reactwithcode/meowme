import React from 'react'
import { Link } from 'react-router-dom';
import {CardColumns, Card, Button} from 'react-bootstrap';
import DetailOverView from '../pages/DetailOverview';


function CardMovie({item}) {


    return (
        <div>
                <div>                
                    <a href={`/DetailOverview/${item.id}`}>
                    <Card style={{ height: '31.625rem', justifyContent:'center', alignContent: 'center'}} className="home-col text-center movie-card" sm={1} md={2} lg={4} id={item.id}>
                     <div className="card-header bg-light"> 
                       <Card.Img style={{ width: '11.813rem', alignSelf: 'center', margin: '15px 0'}} variant="top" src={`https://team-c.gabatch11.my.id/${item.poster}`}/>
                     </div>
                      <Card.Body style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', color: '#000'}}>
                        <div className="card-title">
                          <Card.Title style={{marginTop: '20px'}}><strong>{item.title}</strong></Card.Title>
                        </div>
                        <Card.Text style={{alignSelf: 'center'}}>
                        {
                        item.genre[1][0].toUpperCase() + item.genre[1].slice(1).toLowerCase()
                        }
                        </Card.Text>
                      </Card.Body>
                    </Card>
                    </a>
                </div>

        </div>
    )
}

export default CardMovie
