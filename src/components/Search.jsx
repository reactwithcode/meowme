import React, { useState } from "react";
import {useHistory} from "react-router-dom"
import { useDispatch } from 'react-redux';
import getSearchMovieData from "../store/actions/searchMovie"

const SearchMovie = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const [searchValue, setSearchValue] = useState("");

    const handleSearchInput = event => {
        setSearchValue(event.target.value);
    }

    const resetSearchInput = () => {
        setSearchValue("");
    }

    const submitSearchInput = event => {
        dispatch(getSearchMovieData(searchValue));
        event.preventDefault();
        resetSearchInput();
        history.push("/SearchPage");
    }

    return(
        <>
        <form onSubmit = {submitSearchInput}> 
            <input 
                className= "search"
                type="search" 
                placeholder=" search here..."
                value = {searchValue}
                onChange = {handleSearchInput}
            />
        </form>
        </>    
    )    
}

export default SearchMovie