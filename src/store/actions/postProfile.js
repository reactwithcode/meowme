import axios from "axios"
import {POST_UPDATE_PROFILE_BEGIN, POST_UPDATE_PROFILE_FAIL} from "./actionTypes"

//konidisi awal dari reducer false
const postUpdateProfile = ( email, password,nama, confirmPassword, image ) => dispatch => {
    dispatch({
        type: POST_UPDATE_PROFILE_BEGIN,
        // loading: true,
        error: null
    })
    const token = localStorage.getItem("token")
    axios
    .put(`https://team-c.gabatch11.my.id/user/update/`, {headers:{'Content-Type': 'multipart/form-data',  'AUTHORIZATION': 'Bearer ' + token},
    image,password,confirmPassword,email,nama, 
    })
    .then((res) => 
        console.log('post', res)
        // localStorage.setItem("token", res.data.token)
    )
    .catch((err) =>
        dispatch({
            type: POST_UPDATE_PROFILE_FAIL,
            // loading: false,
            error: err,
        })
    )
  }
  
export default postUpdateProfile;