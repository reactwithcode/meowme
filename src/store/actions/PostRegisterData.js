import axios from "axios"
import {POST_REGISTER_BEGIN, POST_REGISTER_FAIL} from "./actionTypes"

//konidisi awal dari reducer false
const postRegisterData = (nama, email, password, confirmPassword) => dispatch => {
    dispatch({
        type: POST_REGISTER_BEGIN,
        loading: true,
        error: null
    })
    axios
    .post("https://team-c.gabatch11.my.id/user/signup/",
    {
      nama, 
      email,
      password,
      confirmPassword,
    })
    .then((res) => 
    console.log("res", res)
    )
    .catch((err) =>
        dispatch({
            type: POST_REGISTER_FAIL,
            loading: false,
            error: err,
        })
    )
  }
  
export default postRegisterData;