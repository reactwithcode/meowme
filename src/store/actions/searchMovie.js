import axios from "axios"
import {SEARCH_MOVIE_REQUEST, SEARCH_MOVIE_SUCCESS, SEARCH_MOVIE_ERROR} from "./actionTypes"

//konidisi awal dari reducer false
const getSearchMovieData = (searchValue) => dispatch => {
    dispatch({
        type: SEARCH_MOVIE_REQUEST,
        loading: true,
        error: null
    })
    axios
    .get(`https://team-c.gabatch11.my.id/movie/search/?search=${searchValue}`,
    )
    .then((res) => 
        dispatch({
            type: SEARCH_MOVIE_SUCCESS,
            loading: false,
            payload: res.data,
            error: null
        })
    )
    .catch((err) =>
        dispatch({
            type: SEARCH_MOVIE_ERROR,
            loading: false,
            error: err,
        })
    )
  }
  
export default getSearchMovieData;