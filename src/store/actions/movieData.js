
import axios from 'axios';
import movieData from '../reducers/movieData';
import {
    GET_DATA_MOVIE_BEGIN,
    GET_DATA_MOVIE_SUCCESS,
    GET_DATA_MOVIE_FAIL,
    GET_DETAIL_MOVIE_BEGIN,
    GET_DETAIL_MOVIE_SUCCESS,
    GET_DETAIL_MOVIE_FAIL,
    GET_CHARACTERS_MOVIE_BEGIN,
    GET_CHARACTERS_MOVIE_SUCCESS,
    GET_CHARACTERS_MOVIE_FAIL, 
    GET_REVIEWS_MOVIE_BEGIN,
    GET_REVIEWS_MOVIE_SUCCESS,
    GET_REVIEWS_MOVIE_FAIL, 
    POST_RATING_MOVIE_BEGIN,
    POST_RATING_MOVIE_SUCCESS,
    POST_RATING_MOVIE_FAIL, 
    GET_DATA_BY_CATEGORY_BEGIN,
    GET_DATA_BY_CATEGORY_SUCCESS,
    GET_DATA_BY_CATEGORY_FAIL,
} from './actionTypes';


const getDataByCategory = (genre) => dispatch => {
    dispatch({
        type: GET_DATA_BY_CATEGORY_BEGIN,
        loading: true,
        error: null
    })
   axios(`https://team-c.gabatch11.my.id/movie/category/genre=${genre}`, {
    header: {
        "Date": "Fri, 23 Apr 2021 07:42:12 GMT",
        "Content-Type": "application/json; charset=utf-8",
        "Transfer-Encoding": "chunked",
        "Connection": "keep-alive",
        "X-RateLimit-Limit": 100,
        "X-RateLimit-Remaining": 99,
        "X-RateLimit-Reset": 1619163787,
        "X-DNS-Prefetch-Control": "off",
        "Expect-CT": "max-age=0",
        "X-Frame-Options": "SAMEORIGIN",
      }
   })
   .then((res) => 
    dispatch({
        type: GET_DATA_BY_CATEGORY_SUCCESS,
        loading: false,
        payload: res.data.data.movies,
        error: null
    })
   )
   .catch((err) => 
        dispatch({
            type: GET_DATA_BY_CATEGORY_FAIL,
            loading: false,
            error: err
        })
   );
  }



const getDataMovie = () => dispatch => {
    dispatch({
        type: GET_DATA_MOVIE_BEGIN,
        loading: true,
        error: null
    })
   axios('https://team-c.gabatch11.my.id/movie/')
   .then((res) => 
    dispatch({
        type: GET_DATA_MOVIE_SUCCESS,
        loading: false,
        payload: res.data.data.movies,
        error: null
    })
   )
   .catch((err) => 
        dispatch({
            type: GET_DATA_MOVIE_FAIL,
            loading: false,
            error: err
        })
   );
  }

  const getDetailMovie = (id) => dispatch => {
    dispatch({
        type: GET_DETAIL_MOVIE_BEGIN,
        loading: true,
        error: null
    })
   axios(`https://team-c.gabatch11.my.id/movie/movieOne/${id}`, {
       header: {
        "Date": "Fri, 23 Apr 2021 07:42:12 GMT",
        "Content-Type": "application/json; charset=utf-8",
        "Transfer-Encoding": "chunked",
        "Connection": "keep-alive",
        "X-RateLimit-Limit": 100,
        "X-RateLimit-Remaining": 99,
        "X-RateLimit-Reset": 1619163787,
        "X-DNS-Prefetch-Control": "off",
        "Expect-CT": "max-age=0",
        "X-Frame-Options": "SAMEORIGIN",
       }
   })
   .then((res) => 
    dispatch({
        type: GET_DETAIL_MOVIE_SUCCESS,
        loading: false,
        payload: res.data.data,
        error: null
    })
    )
   .catch((err) => 
        dispatch({
            type: GET_DETAIL_MOVIE_FAIL,
            loading: false,
            error: err
        })
        );
       }

    
  const getCharactersMovie = (id) => dispatch => {
    dispatch({
        type: GET_CHARACTERS_MOVIE_BEGIN,
        loading: true,
        error: null
    })
   axios(`https://team-c.gabatch11.my.id/movie/movieCast/${id}`, {
       header: {
        "Date": "Fri, 23 Apr 2021 07:42:12 GMT",
        "Content-Type": "application/json; charset=utf-8",
        "Transfer-Encoding": "chunked",
        "Connection": "keep-alive",
        "X-RateLimit-Limit": 100,
        "X-RateLimit-Remaining": 99,
        "X-RateLimit-Reset": 1619163787,
        "X-DNS-Prefetch-Control": "off",
        "Expect-CT": "max-age=0",
        "X-Frame-Options": "SAMEORIGIN",
       }
   })
   .then((res) => 
    dispatch({
        type: GET_CHARACTERS_MOVIE_SUCCESS,
        loading: false,
        payload: res.data.data.casts,
        error: null
    })
    )
   .catch((err) => 
        dispatch({
            type: GET_CHARACTERS_MOVIE_FAIL,
            loading: false,
            error: err
        })
        );
       }

       const getReviewsMovie = (id) => dispatch => {
        dispatch({
            type: GET_REVIEWS_MOVIE_BEGIN,
            loading: true,
            error: null
        })
       axios(`https://team-c.gabatch11.my.id/movie/movieReviews/${id}`, {
           header: {
            "X-RateLimit-Limit": "100",
            "X-RateLimit-Remaining": "99",
            "Date": "Thu, 22 Apr 2021 16:11:22 GMT",
            "X-RateLimit-Reset": 1619107886,
            "X-DNS-Prefetch-Control": "off",
            "Expect-CT": "max-age=0",
            "X-Frame-Options": "SAMEORIGIN",
            "Strict-Transport-Security": "max-age=15552000; includeSubDomains",
            "X-Download-Options": "noopen",
            "X-Content-Type-Options": "nosniff",
            "X-Permitted-Cross-Domain-Policies": "none",
            "Referrer-Policy": "no-referrer",
            "X-XSS-Protection": 0,
            "Access-Control-Allow-Origin": "*",
            "Content-Type": "application/json; charset=utf-8",
            "Content-Length": "640",
            "ETag": "W/280-+fjsK4iHAzTzPIOLgW0UycbzqJk",
            "Connection": "keep-alive",
            "Keep-Alive": "timeout=5"            
           }
       })
       .then((res) => 
        dispatch({
            type: GET_REVIEWS_MOVIE_SUCCESS,
            loading: false,
            payload: res.data.data.reviews,
            error: null
        })
        )
       .catch((err) => 
            dispatch({
                type: GET_REVIEWS_MOVIE_FAIL,
                loading: false,
                error: err
            })
            );
           }

           const postRatingMovie = (rating, id) => dispatch => {
            dispatch({
                type: POST_RATING_MOVIE_BEGIN,
                loading: true,
                error: null
            })
           axios.post(`https://api.themoviedb.org/3/movie/${id}/rating?api_key=72a71fb379067017f2d6ed3226575c29?Authorization=f1c8f934d229cba200c83fc1f23ff7bd55fc54b5?`, rating, {
                      header: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": "f1c8f934d229cba200c83fc1f23ff7bd55fc54b5",
                        "api-key": "72a71fb379067017f2d6ed3226575c29",
                      },
                      body: {
                        "request_token": "6bc047b88f669d1fb86574f06381005d93d3517a"
                      }
                    })
           .then((res) => 
            dispatch({
                type: POST_RATING_MOVIE_SUCCESS,
                loading: false,
                payload: res.data.results,
                error: null
            })
            )
           .catch((err) => 
                dispatch({
                    type: POST_RATING_MOVIE_FAIL,
                    loading: false,
                    error: err
                })
                );
               }


export  { getDataByCategory, getDataMovie, getDetailMovie, getCharactersMovie, getReviewsMovie, postRatingMovie};

     


