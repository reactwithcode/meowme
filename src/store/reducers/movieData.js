import {
    GET_DATA_MOVIE_BEGIN,
    GET_DATA_MOVIE_SUCCESS,
    GET_DATA_MOVIE_FAIL
} from '../actions/actionTypes';

const initialState = {
    loading: false,
    error: null,
    movies: []
}

const movieData = (state = initialState, action) => {
    const { type, payload, error } = action;
    
    switch (type) {
        default:
            return {
            ...state
            };
        case GET_DATA_MOVIE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null,
            };
        case GET_DATA_MOVIE_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null,
                movies: payload,
            };
        case GET_DATA_MOVIE_FAIL:
            return {
                ...state,
                loading: false,
                error: error,
                movies: []
            }
    }
}


export default movieData;