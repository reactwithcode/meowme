import {
    GET_REVIEWS_MOVIE_BEGIN,
    GET_REVIEWS_MOVIE_SUCCESS,
    GET_REVIEWS_MOVIE_FAIL
} from '../actions/actionTypes';

const initialState = {
    loading: false,
    error: null,
    reviewsMovies: []
}


const reviewsMovieData = (state = initialState, action) => {
    const { type, payload, error } = action;
    
    switch (type) {
        default:
            return {
            ...state
            };
        case GET_REVIEWS_MOVIE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null,
            };
        case GET_REVIEWS_MOVIE_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null,
                reviewsMovies: payload,
            };
        case GET_REVIEWS_MOVIE_FAIL:
            return {
                ...state,
                loading: false,
                error: error,
                reviewsMovies: []
            }
    }
}

export default reviewsMovieData;