import { combineReducers } from 'redux';
import movieData from './movieData';
import detailMovieData from './detailMovieData';
import searchMovieData from "./searchMovieData";
import charsMovieData from "./charactersMovieData";
import reviewsMovieData from "./reviewsMovieData";

const rootReducers = combineReducers({
    movieData,
    detailMovieData,
    searchMovieData,
    charsMovieData,
    reviewsMovieData,
})

export default rootReducers;