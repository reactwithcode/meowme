import React, {useEffect, useState} from 'react';
import Navigation from "../components/Navbar"
import Footer from '../components/Footer';
import BannerDetail from '../components/BannerDetail';
import MainDetailReview from '../components/MainDetailReview';
import './DetailPages.css'

import { useSelector, useDispatch } from 'react-redux';
import {getDetailMovie} from '../store/actions/movieData';

const DetailReview = () => {

    const dispatch = useDispatch();
    const id = window.location.pathname.slice(-24);

    const detailMovieData = useSelector((state) => state.detailMovieData.detailMovies);
  
    useEffect(() => {
        dispatch(getDetailMovie(id));
    }, [])

    const [detailMovie, setDetailMovie] = useState(detailMovieData);
    console.log(detailMovie)

    return(
        <div>
            <Navigation/>
            <BannerDetail/>
            <MainDetailReview/>
            <Footer/>
        </div>
    );
}

export default DetailReview;