import React from "react"
import {useSelector} from "react-redux"
import {Card} from "react-bootstrap"
import Navigation from "../components/Navbar";
import HomeCarousel from "../components/HomeCarousel"
import Footer from "../components/Footer"
import "./Home.css"

const SearchPage = () => {
    const contentSearch = useSelector((state) => state.searchMovieData.movies);
console.log("content", contentSearch)

    const notFound = "Not Found";
    return (
        <>
        <Navigation />
        <HomeCarousel />
        <div>
            {contentSearch && contentSearch.length? contentSearch?.map((item, index) => {
                return (
                <div key={index} className="search-content">
                    <Card.Img className="image-search" src={item.poster? item.poster : "https://www.movienewz.com/img/films/poster-holder.jpg"} alt= "Unavailable"/>
                    <Card.Body>
                    <Card.Title className="image-title">{item.title}</Card.Title>
                    <Card.Text className="image-date">{item.genre}</Card.Text>
                    </Card.Body>
                </div>
                )}): <div className="not-found">{notFound}</div>
            }
        </div>
        <Footer />
        </>
    )
}

export default SearchPage