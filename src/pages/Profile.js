import React, { useEffect, useState } from "react"
import Navigation from "../components/Navbar"
import Footer from "../components/Footer"
import { useDispatch } from "react-redux";
import postUpdateProfile from "../store/actions/postProfile"

//data yg dibuat saat registrasi akan muncul disini => gmna cara get data yg dipost?
//akan melakukan editing pada profile => 
// -apa pakai state dari saat registrasi atau buat baru?

const Profile = () => {
    const dispatch = useDispatch()
    const [profileImage, setProfileImage] = useState("");
    const [previewImage, setPreviewImage] = useState("")
    const [fileImage, setFileImage] = useState(null)
    const [updateName, setUpdateName] = useState("")
    const [updateEmail, setUpdateEmail] = useState("")
    const [updatePassword, setUpdatePassword] = useState("")
    const [updateConfirmPassword, setUpdateConfirmPassword] = useState("")

    const updateProfileImage = event => {
        let displayImage = event.target.files[0]
        if (displayImage) {
            setProfileImage(displayImage);
            setFileImage(displayImage);
        } else {
            setProfileImage(null);
        }
    }
    // preview image from state
    useEffect(() => {
        if (profileImage) {
            const reader = new FileReader();
            reader.onloadend = () => {
                setPreviewImage(reader.result);
            };
            reader.readAsDataURL(profileImage)
        } else {
            setPreviewImage(null)
        }
    },[profileImage])
    console.log("profile",profileImage)

    //send data to server
    const updateProfile = event => {
        event.preventDefault();
        const imgData = new FormData();
        imgData.append("image", fileImage);
        dispatch(postUpdateProfile(imgData));
    }

    const putUpdateName = e => {
        setUpdateName(e.target.value)
    }

    const putUpdateEmail = e => {
        setUpdateEmail(e.target.value)
    }

    const putUpdatePassword = e => {
        setUpdatePassword(e.target.value)
    }

    const putUpdateConfirmPassword = e => {
        setUpdateConfirmPassword(e.target.value)
    }

    return(
        <>
        <Navigation />
        <div className="container-update">
            <label className="tag-profile">Full Name</label>
            <input 
            value={updateName} 
            onChange={putUpdateName}
            type="text" 
            placeholder="full name" 
            className="modal-input"
            />
            <label className="tag-profile">Email</label>
            <input 
            value={updateEmail} 
            onChange={putUpdateEmail}
            type="email" 
            placeholder="email" 
            className="modal-input"/>
            <label className="tag-profile">Password</label>
            <input 
            value={updatePassword} 
            onChange={putUpdatePassword}
            type="password" 
            placeholder="password" 
            className="modal-input"/>
            <label className="tag-profile">Confirm Password</label>
            <input 
            value= {updateConfirmPassword}
            onChange={putUpdateConfirmPassword}
            type="password" 
            placeholder="password" className="modal-input"/>
            <label className="tag-profile">Profile Image</label>
            <form>
            <img 
            src={previewImage}
            alt="profile" className="modal-image-update"/>
            <input 
            onChange={updateProfileImage}
            type="file"  
            accept = "image/*"
            // className="modal-input"
            />
            <button onClick={updateProfile} className= "btn-upload-image">Upload</button>
            </form>
        </div>
        <Footer />
        </>
    )
}

export default Profile;