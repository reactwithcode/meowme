import React from 'react';
import Navigation from "../components/Navbar"
import HomeCarousel from '../components/HomeCarousel';
import Footer from '../components/Footer';
import MovieList from '../components/MovieList';
import './Home.css'

function Home(...props) {

    const {
    dispatch,
    movieData
    } = props;

    return (
        <>
            <Navigation />
            <HomeCarousel />
            <MovieList/>
            <Footer />
        </>
    )
}

export default Home;
